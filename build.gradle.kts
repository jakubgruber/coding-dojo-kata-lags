group = "dk.cngroup.dojo"
version = "0.0.1"


plugins {
    kotlin("jvm") version "1.2.71"
    application
}

repositories {
    jcenter()
}

application {
    mainClassName = "dk.cngroup.dojo.lags.LagsKt"
}

dependencies {
    implementation(kotlin("stdlib", "1.2.71"))
    testImplementation("junit:junit:4.12")
}

val run by tasks.getting(JavaExec::class) {
    standardInput = System.`in`
}