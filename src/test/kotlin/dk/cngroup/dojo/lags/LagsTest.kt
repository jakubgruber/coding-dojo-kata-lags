package dk.cngroup.dojo.lags

import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.io.File

@RunWith(Parameterized::class)
class LagsTest(private val fileName: String, private val expectedResult: Int) {

    companion object {

        @JvmStatic
        @Parameterized.Parameters(name = "GenerateCombinations, calculateProfit - fileName: {0}, expected: {1}")
        fun data(): Collection<Array<Any>> {
            return listOf(
                    arrayOf("original.txt", 18),
                    arrayOf("four.txt", 36),
                    arrayOf("huge_amount.txt", 99),
                    arrayOf("reversed.txt", 26),
                    arrayOf("triple.txt", 31)
            )
        }
    }

    @Test
    fun generateCombinations_calculateProfit() {
        val lags = Lags()

        File("src/test/resources/$fileName").forEachLine {
            lags.requests.add(it.toRequest())
        }

        val combination = lags.getBestCombination()
        val result = lags.calculateProfit(combination)

        assertEquals(expectedResult, result)
    }

}