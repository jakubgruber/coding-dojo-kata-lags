package dk.cngroup.dojo.lags

import java.io.File

fun main(args: Array<String>) {
    println("Enter name of a file to process:")
    val fileName = "src/main/resources/${readLine()}"

    val lags = Lags()

    File(fileName).forEachLine {
        lags.requests.add(it.toRequest())
        println(it)
    }
    println("---------------------")


    val resultList = lags.getBestCombination()
    println("Best combination of requests is: ${resultList.asSequence().map { it.id }.joinToString(prefix = "[", postfix = "]", separator = ", ")}")
    println("Final profit: ${lags.calculateProfit(resultList)}")
}

class Lags {
    val requests: MutableList<Request> = mutableListOf()

    private var allCombinations: MutableList<MutableList<Request>> = mutableListOf()

    fun calculateProfit(list: List<Request>?) = list?.asSequence()?.map { it.price }?.sum() ?: 0

    fun getBestCombination(): List<Request> {
        requests.asSequence().forEach {
            val options = requests.toMutableList()
            generate(it, options, mutableListOf(it))
        }

        return allCombinations.maxBy { list -> list.sumBy { it.price } } ?: emptyList()
    }

    private fun generate(next: Request, options: MutableList<Request>, combination: MutableList<Request>) {
        val filteredOptions = options.asSequence().filter { it.startTime >= next.endTime }.toMutableList()

        if (filteredOptions.isEmpty()) {
            allCombinations.add(combination)
            return
        }

        filteredOptions.asSequence().forEach { current ->
            val newCombination = combination.copy()

            newCombination.add(current)

            val newOptions = filteredOptions.copy()
            newOptions.remove(current)
            generate(current, newOptions, newCombination)
        }
    }

}

data class Request(val id: String, val startTime: Int, val duration: Int, val price: Int) {
    val endTime: Int = startTime + duration
}

fun String.toRequest(): Request {
    val iterator = this.split(" ").iterator()
    return Request(
            id = iterator.next(),
            startTime = iterator.next().toInt(),
            duration = iterator.next().toInt(),
            price = iterator.next().toInt())
}

fun <T> MutableList<T>.copy() = this.asSequence().map { it }.toMutableList()
